package main

import (
	"github.com/gorilla/mux"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"encoding/json"
)

type App struct {
	DB *sql.DB
}

func main() {
	// Initialize App
	app := &App{}

	// Initialize db
	db, err := sql.Open("mysql", "root:root@tcp(app_db:3306)/todo_go")
	// Assign Database to app container
	app.DB = db

	if err != nil {
		panic(err)
	}

	// Initialize Router
	router := mux.NewRouter()

	// Register Routes
	router.HandleFunc("/",app.GetAllTodos).Methods("GET")
	router.HandleFunc("/",app.CreateTodo).Methods("POST")

	router.HandleFunc("/{id}",app.ShowTodo).Methods("GET")
	router.HandleFunc("/{id}",app.UpdateTodo).Methods("POST") // @todo Request must be PUT not POST

	router.HandleFunc("/{id}/delete",app.DeleteTodo).Methods("POST") // @todo Reuqest Must be DELETE not POST and Route Must be /{id} not /{id}/delete

	// Start Server
	http.ListenAndServe(":8000",router)
}

type Message struct {
	Success bool `json:"success"`
	Message string `json:"message"`
}

type Todo struct {
	Id int `json:"id"`
	Title string `json:"title"`
	IsDone bool `json:"is_done"`
}

func (app *App) GetAllTodos(writer http.ResponseWriter, request *http.Request) {
	// Get all rows from database
	rows,err := app.DB.Query("SELECT `id`,`title`,`is_done` FROM `todos`")
	if(err != nil){
		panic("MySQL Connection Error")
	}
	// Prepare rows container
	var todos []*Todo
	// Iterate all rows
	for rows.Next() {
		todo := &Todo{}
		rows.Scan(&todo.Id,&todo.Title,&todo.IsDone)
		todos = append(todos,todo)
	}
	// I don't know what this shit means
	rows.Close()
	json.NewEncoder(writer).Encode(todos)
}

func (app *App) ShowTodo(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	id := params["id"]
	// Prepare todo object
	todo := &Todo{}
	// Get Row By Id
	app.DB.QueryRow("SELECT `id`,`title`,`is_done` FROM `todos` where id = ?", id).Scan(&todo.Id, &todo.Title, &todo.IsDone)
	// Return Response
	json.NewEncoder(writer).Encode(todo)

}

func (app *App) CreateTodo(writer http.ResponseWriter, request *http.Request) {
	// Insert into Database
	_,err := app.DB.Exec("INSERT INTO `todos` (`title`) VALUES (?)",request.FormValue("title"))
	if err != nil {
		panic(err)
	}
	message := &Message{Success:true,Message:"Inserted Successfully"}
	// Return Response
	json.NewEncoder(writer).Encode(message)
}

func (app *App) UpdateTodo(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	id := params["id"]
	// Prepare todo object
	todo := &Todo{}
	// Get Row By Id
	app.DB.QueryRow("SELECT `id`,`title`,`is_done` FROM `todos` where id = ?", id).Scan(&todo.Id, &todo.Title, &todo.IsDone)

	title := request.FormValue("title")
	is_done := request.FormValue("is_done")

	// Update Row
	_,err := app.DB.Exec("UPDATE `todos` SET `title` = ? ,`is_done` = ? WHERE `id` = ?",title,is_done,todo.Id)
	if err != nil {
		panic(err)
	}
	message := &Message{Success:true,Message:"Row Updated Successfully"}
	// Return Response
	json.NewEncoder(writer).Encode(message)
}

func (app *App) DeleteTodo(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	id := params["id"]
	// Prepare todo object
	todo := &Todo{}
	// Get Row By Id
	fetchErr := app.DB.QueryRow("SELECT `id`,`title`,`is_done` FROM `todos` where id = ?", id).Scan(&todo.Id, &todo.Title, &todo.IsDone)
	if fetchErr != nil{
		panic(fetchErr)
	}

	// DELETE Row
	_,err := app.DB.Exec("DELETE FROM `todos` WHERE `id` = ?",todo.Id)
	if err != nil {
		panic(err)
	}
	message := &Message{Success:true,Message:"Row Deleted Successfully"}
	// Return Response
	json.NewEncoder(writer).Encode(message)
}